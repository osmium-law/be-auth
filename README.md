# [OSMIUM] OSMIUM Auth Service
---
*User auth* service untuk OSMIUM dengan menggunakan Google Sign In

## Endpoint
1. **[CREATE TOKEN]** POST   /users/login
	* Function: Membuat token untuk setiap user yang login
    * Content-type:
        * json
    * Request Body:
        | Key       	| Type  			|
        | :---:     	| :-:   			|
        | g_access_token   		| String   			|
		| client_id   	| String		   	|
		| client_secret | String		   	|
	* Response:
        | Key       	| Type  			|
        | :---:     	| :-:   			|
        | access_token   | String   		|
        | refresh_token   | String		   	|

2. **[VERIFY TOKEN]** GET   /users/verify-token/`{str:token}`/
	* Function: Mengecek apakah token valid. Token yang valid adalah token dari pengguna yang telah logged in ke dalam sistem
	* Response:
        * status 200
		* status 409

3. **[CREATE TOKEN]** POST   /users/logout
	* Function: Endpoint untuk user yang logout
    * Content-type:
        * json
    * Request Body:
        | Key       	| Type  			|
        | :---:     	| :-:   			|
        | access_token  | String   			|
        
	* Response:
        | Key       	| Type  			| Value |
        | :---:     	| :-:   			| :-:	|
        | message   | String   		| logout success |

## Deployment
* URL: [osmium-auth](https://osmium-auth.herokuapp.com/)
* DOCS: [osmium-auth docs](https://osmium-auth.herokuapp.com/api/docs/)
	