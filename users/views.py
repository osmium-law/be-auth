from urllib import response
from django.views.decorators.csrf import csrf_exempt
import requests
from django.contrib.auth.models import User
from rest_framework import status
from rest_framework.decorators import api_view, schema
from rest_framework.response import Response
from datetime import datetime
from datetime import timedelta
import hashlib
from django.utils import timezone
from .models import *

import coreapi
from rest_framework.schemas import AutoSchema

import logging
from logstash_async.handler import AsynchronousLogstashHandler
from logstash_async.handler import LogstashFormatter

from django.views.decorators.cache import cache_page

logout_schema = AutoSchema(manual_fields=[
    coreapi.Field("access_token", required=True, location="form", type="string", description="access token here"),
])

login_schema = AutoSchema(manual_fields=[
    coreapi.Field("g_access_token", required=True, location="form", type="string", description="google access token here"),
	coreapi.Field("client_id", required=True, location="form", type="string", description="client id here"),
	coreapi.Field("client_secret", required=True, location="form", type="string", description="client secret here"),
])

# Create the logger and set it's logging level
logger = logging.getLogger("logstash")
logger.setLevel(logging.INFO)        

# Create the handler
handler = AsynchronousLogstashHandler(
    host='34.132.46.126', 
    port=5000, 
    ssl_enable=False, 
    ssl_verify=False,
    database_path='')
# Here you can specify additional formatting on your log record/message
formatter = LogstashFormatter()
handler.setFormatter(formatter)

# Assign handler to the logger
logger.addHandler(handler)

def responseCreator(status = status.HTTP_200_OK, data = None, error = None):
    return Response({
					"data": data,
					"error": error,
				}, status=status)

@cache_page(60 * 120)
@api_view(['GET'])
@csrf_exempt
def verify_token(request, token):
	token_obj = AccessToken.objects.filter(token=token)
	token_exists = token_obj.exists()

	if token_exists:
		token_obj = token_obj[0]
		if token_obj.expired > timezone.now():
			# logger.info("[AUTH]: Verify Token for " +  token_obj.user.username)
			return responseCreator(data={'user': token_obj.user.username})
	# logger.error("[AUTH]: Verify Token: " +  token + " does not exist")
	return responseCreator(error={'message': 'Token is not valid'}, status=status.HTTP_403_FORBIDDEN)

@csrf_exempt
@api_view(['POST'])
@schema(logout_schema)
def logout(request):
	token = request.data.get("access_token")
	AccessToken.objects.filter(token=token).delete()
	logger.info('[AUTH]: Logout')
	return responseCreator()

def verify_google_token(access_token):
	header = {'Authorization': 'Bearer ' + access_token}
	r = requests.get('https://www.googleapis.com/oauth2/v2/userinfo?scope=openid%20profile%20email', headers=header)
	if r.status_code == 200:
		return r.json()
	else:
		return None

def create_token(client, user):
	exp_time = datetime.now() + timedelta(days=1)
	token = client.client_id + user.username + str(exp_time) + 'acc'
	ref_token = client.client_id + user.username + str(exp_time) + 'ref'
	token_hash = hashlib.sha1(token.encode('utf-8')).hexdigest()
	ref_token_hash = hashlib.sha1(ref_token.encode('utf-8')).hexdigest()
	AccessToken.objects.create(
		token = token_hash,
		user = user,
		application = client,
		expired = exp_time
	)
	RefreshToken.objects.create(
		token = ref_token_hash,
		user = user,
		application = client
	)

	return [token_hash, ref_token_hash]

def create_user(data):
	email = data["email"]
	username = email.split('@')[0]
	first_name = data["given_name"]
	last_name = data["family_name"]
	return User.objects.get_or_create(
		email= email,
		username = username,
		first_name = first_name,
		last_name = last_name
	)

@api_view(['POST'])
@csrf_exempt
@schema(login_schema)
def login(request):
	g_access_token = request.data.get('g_access_token')
	client_id = request.data.get('client_id')
	client_secret = request.data.get('client_secret')

	client = RegisteredApp.objects.filter(client_id = client_id, client_secret = client_secret)
	data = verify_google_token(g_access_token)

	if client.exists() and data is not None:
		user = create_user(data)
		tokens = create_token(client[0], user[0])
		data = {'access_token': tokens[0], 'refresh_token': tokens[1]}
		logger.info('[AUTH]: Login: ' + str(user))
		return responseCreator(data=data)
	else:
		logger.error("[AUTH]: Login: " +  g_access_token + " does not exist")
		return responseCreator(error="Request not valid",status=status.HTTP_400_BAD_REQUEST)
