from django.contrib import admin
from .models import *

class RegisteredAppAdmin(admin.ModelAdmin):
    list_display = ('id', 'client_id', 'client_secret')
admin.site.register(RegisteredApp, RegisteredAppAdmin)


admin.site.register(AccessToken)
admin.site.register(RefreshToken)
