from django.urls import path
from . import views

urlpatterns = [
    path('verify-token/<str:token>/', views.verify_token),
	path('logout/', views.logout),
	path('login/', views.login),
]