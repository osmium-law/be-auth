from django.db import models
from django.contrib.auth.models import User

# Create your models here.
class RegisteredApp(models.Model):
	id = models.AutoField(primary_key=True)
	client_id = models.CharField(max_length=500)
	client_secret = models.CharField(max_length=500)

class AccessToken(models.Model):
	id = models.AutoField(primary_key=True)
	token = models.CharField(max_length=500)
	user = models.ForeignKey(User, on_delete=models.CASCADE)
	application = models.ForeignKey(RegisteredApp, on_delete=models.CASCADE)
	expired = models.DateTimeField()

class RefreshToken(models.Model):
	id = models.AutoField(primary_key=True)
	token = models.CharField(max_length=500)
	user = models.ForeignKey(User, on_delete=models.CASCADE)
	application = models.ForeignKey(RegisteredApp, on_delete=models.CASCADE)